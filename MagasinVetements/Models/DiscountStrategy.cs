﻿using MagasinVetements.Models.Clothes;

namespace MagasinVetements.Models.Strategies
{
    public interface IDiscountStrategy
    {
        void Apply(ShopItem item);
        string ToString();
    }

    public abstract class DiscountStrategy
    {
        public float Percentage { get; private set; }

        public DiscountStrategy(float percentage)
        {
            Percentage = percentage;
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }


    public class None : DiscountStrategy, IDiscountStrategy
    {
        public None() : base(0) { }

        public void Apply(ShopItem item)
        {
            item.DicountPrice = item.Price;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Global : DiscountStrategy, IDiscountStrategy
    {
        public Global(float percentage) : base(percentage) { }

        public void Apply(ShopItem item)
        {
            item.DicountPrice = item.Price - (item.Price * Percentage / 100);
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}%)", base.ToString(), Percentage);
        }
    }

    public class Targeted : Global, IDiscountStrategy
    {
        private Seasons season;

        public Targeted(float percentage, Seasons season) : base(percentage) { this.season = season; }

        new public void Apply(ShopItem item)
        {
            if (!item.IsAvailableForSeason(season))
                base.Apply(item);
            else
                item.DicountPrice = item.Price;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", base.ToString(), season.ToString());
        }
    }

    public enum DiscountStrategies
    {
        None = 0,
        Global = 1,
        Targeted = 2
    }
}
