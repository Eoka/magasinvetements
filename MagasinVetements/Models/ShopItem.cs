﻿using System;
using System.ComponentModel;

namespace MagasinVetements.Models.Clothes
{
    public class ShopItem
    {
        public string Description { get; set; }
        public float Price { get; set; }
        public float DicountPrice { get; set; }
        [Browsable(false)]
        public Seasons AvailableSeasonsEnum { get; set; }
        public string Seasons
        {
            get
            {
                string seasons = string.Empty;
                foreach (Seasons season in Enum.GetValues(typeof(Seasons)))
                    if (IsAvailableForSeason(season))
                        seasons += string.Concat(Enum.GetName(typeof(Seasons), season), " ");
                
                return seasons;
            }
        }

        public bool IsAvailableForSeason(Seasons season)
        {
            return (AvailableSeasonsEnum & season) == season;
        }
    }
}
