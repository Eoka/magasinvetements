﻿using System;

namespace MagasinVetements.Models
{
    public enum Seasons
    {
        Winter = 1,
        Spring = 2,
        Summer = 4,
        Fall = 8
    }

    public static class Season
    {
        const int SPRING = 321, SUMMER = 621, FALL = 921, WINTER = 1221;

        public static int GetCurrentSeasonInt()
        {
            int date = (DateTime.Now.Month * 100) + DateTime.Now.Day;

            if (date < SPRING || date >= WINTER)
                return 0;
            if (date < SUMMER)
                return 1;
            if (date < FALL)
                return 2;

            return 3;
        }
    }
}
