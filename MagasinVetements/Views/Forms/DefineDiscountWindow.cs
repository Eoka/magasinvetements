using MagasinVetements.Views.Interfaces;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using MagasinVetements.Args;

namespace MagasinVetements.Views.Forms
{
    public partial class DefineDiscountWindow : Form, IDefineDiscountWindow
    {
        public event EventHandler<DiscountArgs> confirmClickEvent;

        public DefineDiscountWindow()
        {
            InitializeComponent();
        }

        public IList<string> DiscountList
        {
            set
            {
                discountComboBox.Items.Clear();
                foreach (string discount in value)
                    discountComboBox.Items.Add(discount);

                discountComboBox.SelectedItem = value[0];
            }
        }

        public IList<string> SeasonList
        {
            set
            {
                seasonComboBox.Items.Clear();
                foreach (string season in value)
                    seasonComboBox.Items.Add(season);
            }
        }

        public int DefaultSeason
        {
            set
            {
                seasonComboBox.SelectedItem = seasonComboBox.Items[value];
            }
        }

        private void ConfirmButtonClick(object sender, EventArgs e)
        {
            DiscountArgs discountArgs = new DiscountArgs()
            {
                DiscountPlan = discountComboBox.SelectedItem as string,
                Percentage = (float)percentageField.Value,
                Season = seasonComboBox.SelectedItem as string
            };

            if (confirmClickEvent != null)
                confirmClickEvent(this, discountArgs);

            Close();
        }

        private void DiscountSelectedValueChanged(object sender, EventArgs e)
        {
            percentageField.Enabled = discountComboBox.SelectedIndex != 0;
            seasonComboBox.Enabled = discountComboBox.SelectedIndex == 2;
        }
    }
}
