﻿namespace MagasinVetements.Views.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.changeDiscount = new System.Windows.Forms.Button();
            this.addItem = new System.Windows.Forms.Button();
            this.currentDiscountLabel = new System.Windows.Forms.Label();
            this.currentDiscountDisplayLabel = new System.Windows.Forms.Label();
            this.mainDataGridView = new System.Windows.Forms.DataGridView();
            this.mainBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mainDataGridView, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(430, 309);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.Controls.Add(this.changeDiscount, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.addItem, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.currentDiscountLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.currentDiscountDisplayLabel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(424, 29);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // changeDiscount
            // 
            this.changeDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changeDiscount.Location = new System.Drawing.Point(0, 0);
            this.changeDiscount.Margin = new System.Windows.Forms.Padding(0);
            this.changeDiscount.Name = "changeDiscount";
            this.changeDiscount.Size = new System.Drawing.Size(120, 29);
            this.changeDiscount.TabIndex = 0;
            this.changeDiscount.Text = "Change discount plan";
            this.changeDiscount.UseVisualStyleBackColor = true;
            this.changeDiscount.Click += new System.EventHandler(this.OnChangeDiscountClicked);
            // 
            // addItem
            // 
            this.addItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addItem.Location = new System.Drawing.Point(344, 0);
            this.addItem.Margin = new System.Windows.Forms.Padding(0);
            this.addItem.Name = "addItem";
            this.addItem.Size = new System.Drawing.Size(80, 29);
            this.addItem.TabIndex = 1;
            this.addItem.Text = "Add item";
            this.addItem.UseVisualStyleBackColor = true;
            this.addItem.Click += new System.EventHandler(this.OnAddItemClicked);
            // 
            // currentDiscountLabel
            // 
            this.currentDiscountLabel.AutoSize = true;
            this.currentDiscountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentDiscountLabel.Location = new System.Drawing.Point(120, 0);
            this.currentDiscountLabel.Margin = new System.Windows.Forms.Padding(0);
            this.currentDiscountLabel.Name = "currentDiscountLabel";
            this.currentDiscountLabel.Size = new System.Drawing.Size(70, 29);
            this.currentDiscountLabel.TabIndex = 2;
            this.currentDiscountLabel.Text = "Current plan :";
            this.currentDiscountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // currentDiscountDisplayLabel
            // 
            this.currentDiscountDisplayLabel.AutoSize = true;
            this.currentDiscountDisplayLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentDiscountDisplayLabel.Location = new System.Drawing.Point(190, 0);
            this.currentDiscountDisplayLabel.Margin = new System.Windows.Forms.Padding(0);
            this.currentDiscountDisplayLabel.Name = "currentDiscountDisplayLabel";
            this.currentDiscountDisplayLabel.Size = new System.Drawing.Size(150, 29);
            this.currentDiscountDisplayLabel.TabIndex = 3;
            this.currentDiscountDisplayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainDataGridView
            // 
            this.mainDataGridView.AllowUserToAddRows = false;
            this.mainDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGridView.Location = new System.Drawing.Point(0, 35);
            this.mainDataGridView.Margin = new System.Windows.Forms.Padding(0);
            this.mainDataGridView.Name = "mainDataGridView";
            this.mainDataGridView.Size = new System.Drawing.Size(430, 274);
            this.mainDataGridView.TabIndex = 1;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 309);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainWindow";
            this.Text = "Shop Manager";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button changeDiscount;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.Label currentDiscountLabel;
        private System.Windows.Forms.Label currentDiscountDisplayLabel;
        private System.Windows.Forms.DataGridView mainDataGridView;
        private System.Windows.Forms.BindingSource mainBindingSource;
    }
}

