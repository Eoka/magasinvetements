﻿using MagasinVetements.Args;
using MagasinVetements.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MagasinVetements.Views.Forms
{
    public partial class AddShopItemWindow : Form, IAddShopItemWindow
    {
        public event EventHandler<ItemArgs> addButtonClickEvent;

        IList<string> IAddShopItemWindow.SeasonList
        {
            set
            {
                seasonBox.Items.Clear();
                foreach (string season in value)
                    seasonBox.Items.Add(season);
            }
        }

        public AddShopItemWindow()
        {
            InitializeComponent();
        }

        private void OnAddButtonClick(object sender, EventArgs e)
        {
            string[] availableSeasons = new string[seasonBox.CheckedItems.Count];

            for(int i = 0; i < availableSeasons.Length; i++)
                availableSeasons[i] = seasonBox.CheckedItems[i] as string;

            ItemArgs itemArgs = new ItemArgs()
            {
                Description = descriptionField.Text,
                Price = (float)priceField.Value,
                AvailableSeasons = availableSeasons
            };

            if (addButtonClickEvent != null)
                addButtonClickEvent(sender, itemArgs);

            Close();
        }
    }
}
