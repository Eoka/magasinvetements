﻿using MagasinVetements.Views.Interfaces;
using System;
using System.Collections;
using System.Windows.Forms;

namespace MagasinVetements.Views.Forms
{
    public partial class MainWindow : Form, IMainWindow
    {
        public event EventHandler changeDiscountClickEvent;
        public event EventHandler addItemClickEvent;

        public string CurrentDiscountDisplayLabel
        {
            set
            {
                currentDiscountDisplayLabel.Text = value;
            }
        }


        public MainWindow()
        {
            InitializeComponent();
        }

        public void BindToData(IList data)
        {
            mainBindingSource.DataSource = data;
            mainBindingSource.ResetBindings(true);
            mainDataGridView.DataSource = mainBindingSource;
        }

        public void AddItemToBinding(object item)
        {
            mainBindingSource.Add(item);
        }

        protected void OnChangeDiscountClicked(object sender, EventArgs args)
        {
            if (changeDiscountClickEvent != null)
                changeDiscountClickEvent(sender, args);
        }

        protected void OnAddItemClicked(object sender, EventArgs args)
        {
            if (addItemClickEvent != null)
                addItemClickEvent(sender, args);
        }
    }
}
