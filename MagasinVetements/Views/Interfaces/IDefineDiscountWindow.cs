﻿using MagasinVetements.Args;
using System;
using System.Collections.Generic;

namespace MagasinVetements.Views.Interfaces
{
    public interface IDefineDiscountWindow : IWindow
    {
        event EventHandler<DiscountArgs> confirmClickEvent;

        IList<string> DiscountList { set; }
        IList<string> SeasonList { set; }
        int DefaultSeason { set; }
    }
}
