﻿using MagasinVetements.Args;
using System;
using System.Collections.Generic;

namespace MagasinVetements.Views.Interfaces
{
    public interface IAddShopItemWindow : IWindow
    {
        event EventHandler<ItemArgs> addButtonClickEvent;
        IList<string> SeasonList { set; }
    }
}
