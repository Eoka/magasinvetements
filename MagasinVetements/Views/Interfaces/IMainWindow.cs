﻿using System;
using System.Collections;

namespace MagasinVetements.Views.Interfaces
{
    public interface IMainWindow : IWindow
    {
        event EventHandler changeDiscountClickEvent;
        event EventHandler addItemClickEvent;

        string CurrentDiscountDisplayLabel { set; }

        void BindToData(IList data);
        void AddItemToBinding(object item);
    }
}
