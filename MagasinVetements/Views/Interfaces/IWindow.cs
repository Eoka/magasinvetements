﻿using System.Windows.Forms;

namespace MagasinVetements.Views.Interfaces
{
    public interface IWindow
    {
        event FormClosingEventHandler FormClosing;
        void Show();
        void Close();
    }
}
