﻿using MagasinVetements.AppController;
using MagasinVetements.Models;
using MagasinVetements.Models.Strategies;
using MagasinVetements.Views.Forms;
using MagasinVetements.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MagasinVetements.Forms.Controllers
{
    public class DefineDiscountController : Controller
    {
        private IDefineDiscountWindow window;
        public IDefineDiscountWindow Window
        {
            get { return window; }

            set
            {
                window = value;
                window.FormClosing += FormHasClosed;
            }
        }

        private MainWindowController mainWindowController;

        public override void HandleNavigation(object args)
        {
            InitWindow();
            if (args is MainWindowController)
                mainWindowController = args as MainWindowController;
            Window.Show();
        }

        private void InitWindow()
        {
            var discountList = new List<string>();
            var seasonList = new List<string>();

            foreach (string discount in Enum.GetNames(typeof(DiscountStrategies)))
                discountList.Add(discount);

            foreach (string season in Enum.GetNames(typeof(Seasons)))
                seasonList.Add(season);
            
            Window.DiscountList = discountList;
            Window.SeasonList = seasonList;
            Window.DefaultSeason = Season.GetCurrentSeasonInt();
        }

        public void FormHasClosed(object sender, FormClosingEventArgs args)
        {
            var newWindow = new DefineDiscountWindow();
            Window = newWindow;
            newWindow.confirmClickEvent += mainWindowController.OnDiscountPlanChanged;
        }
    }
}
