﻿using MagasinVetements.AppController;
using MagasinVetements.Views.Interfaces;
using MagasinVetements.Views.Forms;
using MagasinVetements.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MagasinVetements.Forms.Controllers
{
    public class AddShopItemController : Controller
    {
        private IAddShopItemWindow window;
        public IAddShopItemWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.FormClosing += FormHasClosed;
            }
        }

        private MainWindowController mainWindowController;

        public override void HandleNavigation(object args)
        {
            InitWindow();
            if (args is MainWindowController)
                mainWindowController = args as MainWindowController;
            Window.Show();
        }

        private void InitWindow()
        {
            var seasonList = new List<string>();
            foreach (string season in Enum.GetNames(typeof(Seasons)))
                seasonList.Add(season);
            Window.SeasonList = seasonList;
        }

        public void FormHasClosed(object sender, FormClosingEventArgs args)
        {
            var newWindow = new AddShopItemWindow();
            Window = newWindow;
            newWindow.addButtonClickEvent += mainWindowController.OnItemAdded;
            
        }
    }
}
