﻿using MagasinVetements.AppController;
using MagasinVetements.Views.Interfaces;
using System;
using MagasinVetements.Args;
using MagasinVetements.Models.Clothes;
using System.Collections.Generic;
using MagasinVetements.Models.Strategies;

namespace MagasinVetements.Forms.Controllers
{
    public class MainWindowController : Controller
    {
        IMainWindow window;
        public IMainWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.addItemClickEvent += AddItemEventHandler;
                window.changeDiscountClickEvent += changeDiscountEventHandler;
                window.CurrentDiscountDisplayLabel = currentStrategy.ToString();
                window.BindToData(itemList);
            }
        }

        private List<ShopItem> itemList;
        private IDiscountStrategy currentStrategy;

        public MainWindowController()
        {
            itemList = new List<ShopItem>();
            currentStrategy = new None();
        }

        public override void HandleNavigation(object args)
        {

        }

        public void OnItemAdded(object sender, ItemArgs e)
        {
            var item = e.MakeItem();
            currentStrategy.Apply(item);
            Window.AddItemToBinding(item);
        }

        internal void OnDiscountPlanChanged(object sender, DiscountArgs e)
        {
            currentStrategy = e.MakeStrategy();
            window.CurrentDiscountDisplayLabel = currentStrategy.ToString();
            foreach (ShopItem item in itemList)
                currentStrategy.Apply(item);
            Window.BindToData(itemList);
        }

        private void changeDiscountEventHandler(object sender, EventArgs args)
        {
            RequestNavigationTo("DefineDiscount", this);
        }

        private void AddItemEventHandler(object sender, EventArgs args)
        {
            RequestNavigationTo("AddShopItem", this);
        }
    }
}
