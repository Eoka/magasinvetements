﻿using MagasinVetements.Models;
using MagasinVetements.Models.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagasinVetements.Args
{
    public class DiscountArgs : EventArgs
    {
        public string DiscountPlan { get; set; }
        public float Percentage { get; set; }
        public string Season { get; set; }

        public IDiscountStrategy MakeStrategy()
        {
            DiscountStrategies strategy = (DiscountStrategies)Enum.Parse(typeof(DiscountStrategies), DiscountPlan);
            switch(strategy)
            {
                case DiscountStrategies.Global:
                    return new Global(Percentage);
                case DiscountStrategies.Targeted:
                    return new Targeted(Percentage, (Seasons)Enum.Parse(typeof(Seasons), Season));
                default:
                    return new None();
            }
        }
    }
}
