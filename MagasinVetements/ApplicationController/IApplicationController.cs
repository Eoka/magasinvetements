﻿using System.Collections.Generic;

namespace MagasinVetements.AppController
{
    public interface IApplicationController
    {
        ICollection<string> ControllersName { get; }
        void NavigateTo(string controllerName, object args);
        void Register(Controller controller);
    }
}
