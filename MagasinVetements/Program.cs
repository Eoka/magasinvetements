﻿using MagasinVetements.AppController;
using MagasinVetements.Forms.Controllers;
using MagasinVetements.Views.Forms;
using System;
using System.Windows.Forms;

namespace MagasinVetements
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetupApplication();
            Application.Run(SetupControllers());
        }

        private static Form SetupControllers()
        {
            var appController = new ApplicationController();

            var mainWindow = new MainWindow();
            var mainWindowController = new MainWindowController() { Window = mainWindow };
            appController.Register(mainWindowController);

            var defineDiscountWindow = new DefineDiscountWindow();
            var defineDiscountController = new DefineDiscountController() { Window = defineDiscountWindow };
            defineDiscountWindow.confirmClickEvent += mainWindowController.OnDiscountPlanChanged;
            appController.Register(defineDiscountController);

            var addShopItemWindow = new AddShopItemWindow();
            var addShopItemController = new AddShopItemController() { Window = addShopItemWindow };
            addShopItemWindow.addButtonClickEvent += mainWindowController.OnItemAdded;
            appController.Register(addShopItemController);

            return mainWindow;
        }

        private static void SetupApplication()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
